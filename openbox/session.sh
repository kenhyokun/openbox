#!/bin/bash
curr_project_dir=~/development-project/cpp/raylib/tes2

feh --bg-scale ~/.config/openbox/oldschoolbg.png
xfce4-terminal -e htop --geometry=58x18+8+110 -T "htop" &
xfce4-terminal -e ./date.sh --geometry=30x3+8+17 -T "Date & time" &
xfce4-terminal --geometry 80x25-11+110 --working-directory=$curr_project_dir -T "Curr Project"



